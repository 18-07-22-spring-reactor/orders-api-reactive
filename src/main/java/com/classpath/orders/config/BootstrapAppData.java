package com.classpath.orders.config;

import java.util.stream.IntStream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import com.classpath.orders.dao.OrderDAO;
import com.classpath.orders.model.Order;
import com.classpath.orders.service.OrderService;

import lombok.RequiredArgsConstructor;
import net.datafaker.Faker;

@Configuration
@RequiredArgsConstructor
@org.springframework.core.annotation.Order(100)
public class BootstrapAppData implements CommandLineRunner{
	
	private final OrderService orderService;
	private final Faker faker = new Faker();

	
	public void insertOrders() {
		//TODO
		
	}

	@Override
	public void run(String... args) throws Exception {
		

		IntStream.range(0, 5)
				.forEach(value -> {
					Order order = Order.builder()
										.email(faker.internet().emailAddress())
										.name(faker.name().firstName())
										.id(faker.number().randomNumber())
										.build();
					this.orderService.saveOrder(order);
				});
		
		
	}

}
