package com.classpath.orders;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.messaging.Source;

import com.classpath.orders.channel.InputChannel;
import com.classpath.orders.channel.OutputChannel;

@SpringBootApplication
//@EnableBinding({OutputChannel.class, InputChannel.class})
public class OrdersApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdersApiApplication.class, args);
	}
}
