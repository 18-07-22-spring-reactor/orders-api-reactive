package com.classpath.orders.service;

import java.util.function.Consumer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.classpath.orders.model.Order;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class OrderProcess {

	/*
	 * @StreamListener(value = InputChannel.ORDERS_IN) public void
	 * processOrder(Order order) { log.info("processing the orders ::::");
	 * System.out.println("*************************************");
	 * System.out.println(order);
	 * System.out.println("*************************************"); }
	 */

	@Bean
	public Consumer<Order> input() {
		return order -> {
			/*
			 * System.out.
			 * println("Processing the order using Functional style programming....");
			 * System.out.println("*************************************");
			 * System.out.println(order);
			 * System.out.println("*************************************");
			 */
		};
	}
}
