package com.classpath.orders.service;

import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;

import com.classpath.orders.dao.OrderDAO;
import com.classpath.orders.model.Order;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class OrderService {
	
	private final OrderDAO orderDAO;
	//private final OutputChannel source;
	private final StreamBridge streamBridge;
	
	
	public Mono<Order> saveOrder(Order order) {
		Mono<Order> savedOrder = this.orderDAO.saveOrder(order);
		savedOrder.subscribe(o -> {
		//	Message<Order> orderMessage = MessageBuilder.withPayload(o).build();
		//	this.source.output().send(orderMessage);
		});
		this.streamBridge.send("orders-out-0", order);
		return savedOrder;
	}
	
	public Flux<Order> fetchAllOrders(){
		return this.orderDAO.fetchAllOrders();
	}
	
	public Mono<Order> findOrderByOrderId(long id) {
		return this.orderDAO
				   .fetchOrderById(id);
	}
	
	public Mono<Void> deleteOrderById(long orderId) {
		return this.orderDAO.deleteOrderById(orderId);
	}
}
