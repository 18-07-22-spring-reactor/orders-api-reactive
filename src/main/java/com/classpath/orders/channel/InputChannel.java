package com.classpath.orders.channel;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface InputChannel {
	
	/**
	 * Input channel name.
	 */
	String ORDERS_IN = "orders-in";

	/**
	 * @return input channel.
	 */
	@Input(InputChannel.ORDERS_IN)
	SubscribableChannel input();

}
