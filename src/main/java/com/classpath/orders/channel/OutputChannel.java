package com.classpath.orders.channel;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;

public interface OutputChannel {
	
	/**
	 * Name of the output channel.
	 */
	String ORDERS_OUT = "orders-out";

	/**
	 * @return output channel
	 */
	@Output(OutputChannel.ORDERS_OUT)
	MessageChannel output();


}
