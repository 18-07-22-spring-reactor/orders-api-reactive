package com.classpath.orders.dao;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.classpath.orders.model.Order;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public class OrderDAO {
	
	private Set<Order> orders = new HashSet<>();
	
	public Mono<Order> saveOrder(Order order) {
		this.orders.add(order);
		return Mono.just(order);
	}
	
	public Flux<Order> fetchAllOrders(){
		return Flux.fromIterable(orders);
	}
	
	public Mono<Order> fetchOrderById(long orderId){
		Optional<Order> optionalOrder = this.orders.stream().filter(order -> order.getId() == orderId).findAny();
		return optionalOrder.isPresent() ? Mono.just(optionalOrder.get()) : Mono.empty();
	}
	
	public Mono<Void> deleteOrderById(long orderId) {
		this.orders.removeIf(order -> order.getId() == orderId);
		return Mono.empty();
	}

}
