package com.classpath.orders.client;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.classpath.orders.model.Order;
import com.classpath.orders.service.OrderService;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;

@Component
@RequiredArgsConstructor
@org.springframework.core.annotation.Order(110)
public class OrderClient implements CommandLineRunner {
	
	private final OrderService orderService;

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Application is started ........");
		
		System.out.println("Invoking a REST endpoint :: ");
		//Flux<Order> orders = this.orderService.fetchAllOrders();
		/*
		 * System.out.println("Size of all orders :: "+ orders.size());
		 * orders.stream().forEach(order -> System.out.println(order.getName()));
		 */
		//orders.subscribe(o -> System.out.println(o.getName()));
		
		Thread.sleep(Long.MAX_VALUE);
	}
}
