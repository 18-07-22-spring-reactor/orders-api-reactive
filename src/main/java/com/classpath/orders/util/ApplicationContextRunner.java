package com.classpath.orders.util;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Component
public class ApplicationContextRunner implements CommandLineRunner{

	private final ApplicationContext applicationContext;
	
	public ApplicationContextRunner(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
	@Override
	public void run(String... args) throws Exception {
		/*
		 * Non reactive approach
		 * String [] beanNames = this.applicationContext.getBeanDefinitionNames();
		 * System.out.println("================Bean names==================");
		 * Stream.of(beanNames).forEach(bean -> System.out.println(bean));
		 * System.out.println("================Bean names==================");
		 */
		
		//Reactive approach
		/*
		 * System.out.println("================Bean names=================="); String []
		 * beanNames = this.applicationContext.getBeanDefinitionNames();
		 * Flux.fromArray(beanNames).subscribe(bean -> { System.out.println(bean); });
		 * System.out.println("================Bean names==================");
		 */
		
	}

}
